package Network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;


class History {
    private List<String> history = new CopyOnWriteArrayList<>();
    void addHistoryEl(String el) {
        if (history.size() >= 15) {
            history.remove(0);
            history.add(el);
        } else {
            history.add(el);
        }
    }
}

public class MainServer {
    private static final int PORT = 8080;
    static List<SubServer> subServers = new CopyOnWriteArrayList<>();
    static History history;
    static Random random = new Random();

    public static void main(String[] args) throws IOException {
        try (ServerSocket server = new ServerSocket(PORT)) {
            history = new History();
            System.out.println("Server created");
            String answer = String.valueOf(random.nextInt(10));
            System.out.println("ANSWER = "+ answer);

            while (true) {
                //блокируется до возникновения нового соединения
                Socket socket = server.accept();
                try {
                    subServers.add(new SubServer(socket, answer));
                } catch (IOException e) {
                    socket.close();
                }
            }
        }
    }
}