package Network;

import java.io.*;
import java.net.Socket;

class SubServer extends Thread {

    private Socket socket; // сокет, через который сервер общается с клиентом
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток завписи в сокет
    private String answer;

    SubServer(Socket socket, String answer) throws IOException {
        this.socket = socket;
        this.answer = answer;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    @Override
    public void run() {
        String entry, nick;
        try {
            nick = in.readLine();
            out.write("Welcome to the game " + nick + "\n");
            out.flush();
            try {
                while (true) {
                    entry = in.readLine();
                    System.out.println(entry);
                    MainServer.history.addHistoryEl(entry);
                    System.out.println(nick + " " + entry);
                    send(entry);
                    if(entry.equals(answer)){
                        //разослать сообщение по всем клиентам
                        //synchronized(MainServer.subServers) {
                            for (SubServer vr : MainServer.subServers) {
                                vr.send(nick + " win\nStart a new game");
                            }
                        //}
                        answer = String.valueOf(MainServer.random.nextInt(10));
                        System.out.println("ANSWER = " + answer);
                    }else{
                        send("Try again");
                    }
                }
            } catch (NullPointerException ignored) {
            }
        } catch (IOException e) {
            this.downService();
        }
    }

    private void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {
        }

    }

    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (SubServer el : MainServer.subServers) {
                    if (el.equals(this)) el.interrupt();
                    MainServer.subServers.remove(this);
                }
            }
        } catch (IOException ignored) {
        }
    }
}
